# Copyright (c) 2007 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Xorg video driver for cirrus"

LICENCES="X11"
SLOT="0"
PLATFORMS="~amd64 ~x86"

DEPENDENCIES="
    build:
        x11-proto/xorgproto
    build+run:
        x11-libs/libpciaccess[>=0.8.0]
        x11-server/xorg-server[>=1.4.0]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Disable-acceleration-under-qemu.patch
    "${FILES}"/0002-Use-16bpp-when-running-in-virt-and-on-XenSource-gfx.patch
    "${FILES}"/0003-Remove-almost-no-op-setup-functions.patch
    "${FILES}"/0004-Don-t-build-split-alpine-and-laguna-support.patch
    "${FILES}"/0005-alpine-Default-to-16bpp.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-xaa
)

pkg_setup() {
    # Driver uses xf86LoadSubModule to load another xorg module at runtime which means that it
    # contains undefined symbols which would cause loading it to fail when built with -Wl,-z,now
    LDFLAGS+=" -Wl,-z,lazy"
}

